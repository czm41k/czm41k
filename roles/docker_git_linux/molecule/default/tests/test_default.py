import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
# print(os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_git_installed(host):
    cmd = host.run('git --version')
    assert cmd.stdout is not None, 'No response for git --version'
    assert cmd.rc == 0, 'Non zero response code for git --version'


def test_git_user(host):
    cmd = host.run('git config --global user.name')
    assert cmd.stdout == 'CSI Test', \
        f'Wrong user.name configured for Git : {cmd.stdout}'
